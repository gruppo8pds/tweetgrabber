package test;

import static org.junit.Assert.*;

import org.junit.Test;

import util.AlarmWords;

public class AlarmWordsTest {

	@Test
	public void testStaticAlarmWords() {
		
		assertNotNull("L'istanza statica di AlarmWords non � stata creata correttamente.", AlarmWords.alarmWords);
	}
	
	@Test
	public void testNotNullAlarmWords() {

		AlarmWords alarmWordsTest = new AlarmWords();
		
		assertNotNull("L'istanza di AlarmWords non � stata creata correttamente.", alarmWordsTest);
	}

	@Test
	public void testAddAlarmWord() {
		
		int listSize = 3;
		
		AlarmWords.alarmWords.addAlarmWord("prova");
		AlarmWords.alarmWords.addAlarmWord("covid");
		AlarmWords.alarmWords.addAlarmWord("terremoto");
		
		assertEquals("La lista non contiene tutte le parole che dovrebbero essere state aggiunte", 
				listSize, AlarmWords.alarmWords.getAlarmWordsList().size());
		
		assertEquals("La lista non contiene la parola prevista alla posizione specificata",
				AlarmWords.alarmWords.getAlarmWordsList().get(1), "covid");
	}

	@Test
	public void testRemoveAlarmWord() {
		
		AlarmWords.alarmWords.removeAlarmWord("prova");
		
		assertFalse("La parola non � stata rimossa correttamente dalla lista.", 
				AlarmWords.alarmWords.getAlarmWordsList().contains("prova"));
	}

	@Test
	public void testGetAlarmWordsList() {
		
		assertNotNull("La lista non � stata restituita correttamente dal metodo.", AlarmWords.alarmWords.getAlarmWordsList());
	}

}
