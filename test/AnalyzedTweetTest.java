package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import org.junit.jupiter.api.Test;

import twitter4j.Status;
import util.AnalyzedTweet;

class AnalyzedTweetTest {
	
	AnalyzedTweet at = new AnalyzedTweet();
	List<Status> list = null;
	
	@FXML
	private Label lblPercent = new Label();
	
	@Test
	void testPercentMedia() {
		at.percentMedia(list, lblPercent);
		assertEquals("Lista non nulla", "0 %", lblPercent.getText());
	}

}
