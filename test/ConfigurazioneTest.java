package test;


import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import util.Configurazione;

class ConfigurazioneTest {
	
	Configurazione cf = new Configurazione();
	
	@Test
	void testLeggiToken() {
		try {
		cf.leggiToken();
		}catch(Exception e) {
			fail("Errore durante la lettura del file conf.txt");
		}
	}

}
