package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import core.AlarmHandler;
import util.AlarmWords;

public class AlarmHandlerTest {
	
	AlarmHandler alarmHandler;
	
	@Mock
	AlarmHandler mockAlarm;
	
	@Before
	public void init() {
		
		alarmHandler = new AlarmHandler();
		
		AlarmWords.alarmWords.addAlarmWord("prova");
		AlarmWords.alarmWords.addAlarmWord("covid");
		AlarmWords.alarmWords.addAlarmWord("terremoto");
	}

	@Test
	public void testSetEventStartTime() {
		
		long startTime = System.currentTimeMillis();
		
		alarmHandler.setEventStartTime(startTime);
		
		assertEquals("La variabile eventStartTime non � stata inizializzata correttamente dal metodo.",
				startTime, alarmHandler.getEventStartTime());
	}

	@Test
	public void testCheckAlarmConditionTrue() {
		
		alarmHandler.setEventStartTime(System.currentTimeMillis());

		boolean resTrue = alarmHandler.checkAlarmCondition("prova", 60, System.currentTimeMillis());
		
		assertTrue("Il controllo della condizione d'allarme ha restituito falso invece di vero.", resTrue);
		
	}
	
	@Test
	public void testCheckAlarmConditionFalse() {
		
		alarmHandler.setEventStartTime(System.currentTimeMillis());

		boolean resFalse = alarmHandler.checkAlarmCondition("prova", 20, System.currentTimeMillis());
		
		assertFalse("Il controllo della condizione d'allarme ha restituito vero nonostante il basso numero di tweet dato.", resFalse);
		
		resFalse = alarmHandler.checkAlarmCondition("prova", 70, System.currentTimeMillis()+10000);
		
		assertFalse("Il controllo della condizione d'allarme ha restituito vero nonostante il lasso di tempo troppo grande", resFalse);
		
	}

	@Test
	public void testActivateAlarmTrue() {
		
		mockAlarm = Mockito.mock(AlarmHandler.class);
		
		Mockito.when(mockAlarm.activateAlarm()).thenReturn(true);
		
		boolean choice = mockAlarm.activateAlarm();
		
		assertTrue("L'allarme ha restituito falso invece di vero.", choice);
		
	}
	
	@Test
	public void testActivateAlarmFalse() {
		
		mockAlarm = Mockito.mock(AlarmHandler.class);
		
		Mockito.when(mockAlarm.activateAlarm()).thenReturn(false);
		
		boolean choice = mockAlarm.activateAlarm();
		
		assertFalse("L'allarme ha restituito vero invece di falso.", choice);
		
	}

}
