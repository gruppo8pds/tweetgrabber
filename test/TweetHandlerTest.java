package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import core.MapHandler;
import core.TweetHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import twitter4j.Query;
import twitter4j.Status;
import util.Position;

public class TweetHandlerTest {
	
	private TweetHandler tweetHandler;
	private ObservableList<String> tweetsObsList;
	private MapHandler mapHandler;
	private final String test_words[] = {"covid", "", "#gatto", "23"};
	private final Position positionRoma = new Position (41.902782, 12.496366);
	@Before
	public void init() {
		tweetHandler = new TweetHandler();
		tweetsObsList = FXCollections.observableList(new ArrayList<String>());
	}
    
	@Test
	public void testNotNullTweetList() {
		
		for (String search_term: test_words ) {	
			
			JsonArray tweetList = tweetHandler.search(search_term, positionRoma);
		
			assertNotNull("La lista dei tweet 锟� vuota", tweetList);
		
		}
	}
	
	@Test
	public void testContentTweetList() {
		
		int num_tests = 3;
		
		for (String search_term: test_words ) {	
			
			JsonArray tweetList = tweetHandler.search(search_term, positionRoma);
			
			for (int i = 0; i < num_tests; i++) {
				
				JsonElement tweet = tweetList.get(i);
				
				String tweet_text = tweet.getAsJsonObject().get("text").toString().toLowerCase();
				
				//System.out.println(tweet_text);

				assertTrue("Il tweet non contiene la parola prevista: " + search_term + "", tweet_text.contains(search_term));
			
			}
		}
	}
	
	
	@Test
	public void testNotNullQuery() {
		
		for (String search_term: test_words ) {	
			
			Query query = new Query(search_term);
		
			assertNotNull("La query non 锟� stata creata correttamente", query);
		}
		
	}
	
	@Test
	public void testQuerySize() {
		
		for (String search_term: test_words ) {	
		
			Query query = new Query(search_term);
			
			final int tweetCount = 100;
			query.setCount(tweetCount);
			
			assertEquals("La query non ha impostato correttamente il numero di tweet da restituire", tweetCount, query.getCount());
		}
	}
	
	@Test
	public void testStreamTweetNotNull() {
		for (String search_term : test_words) {
			tweetHandler.startStreamSearch(search_term, tweetsObsList);
		}
		JsonArray jsonarray = tweetHandler.stopStreamSearch();
		assertNotNull("Il tweet stream search non funziona", jsonarray);
	}
	
	@Test 
	public void testSearchStatus() {
		Status status = tweetHandler.searchById("1050118621198921728");
		assertNotNull("searchById non funziona", status.getText());
	}
}
