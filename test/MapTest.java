package test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import com.google.gson.JsonArray;

import core.MapHandler;
import core.TweetHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import twitter4j.GeoLocation;
import util.Position;

class MapTest {
	private MapHandler mapHandler = new MapHandler();
	private GeoLocation[][] geolocation = new GeoLocation[][] {{new GeoLocation(11.3, 32.4),new GeoLocation(22.5, 12.7),new GeoLocation(24.6, 8.9),new GeoLocation(4.2, 9.9)},{}};
	private TweetHandler tweetHandler;
	private ObservableList<String> tweetsObsList;
	private final String test_words[] = {"covid", "", "#gatto", "scuola"};
	private final Position positionRoma = new Position (41.902782, 12.496366);
	@Before
	public void init() {
		tweetHandler = new TweetHandler();
		tweetsObsList = FXCollections.observableList(new ArrayList<String>());
	}
	
	@Test
	void testToPosition() {
		Position testPos = mapHandler.toPosition(geolocation, "testing location");
		assertNotNull("Il metodo non funziona come deve fare", testPos);
	}

	@Test
	void testTweetlistPosition(){
		JsonArray tweetList = null;
		tweetHandler = new TweetHandler();
		for (String search_term: test_words ) {	
			tweetList = tweetHandler.search(search_term, positionRoma);
		}
		mapHandler.updateJsonTweetList(tweetList);
		mapHandler.initializeTweetPositions();
		assertNotEquals(0, mapHandler.getPositionList().size(), "le posizioni non sono state inizializzate correttamente");
	}
}
