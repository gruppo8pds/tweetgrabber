package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import control.MainController;
import javafx.stage.Stage;
import main.Main;

class mainTest {

	private Main main;
	private Stage stage;
	private MainController mainController;
	private Thread mainThread;
	private final String test_word = "covid";

    @Before
	@Test
	public void testMainInit() throws Exception {
		main = new Main();
		main.init();
		assertNotNull("Il main viene inizializzata correttamente", main);
		mainThread = new Thread() {
			@Override
			public void run() {
				main.main(null);
			}
		};

	}
}
