# Gruppo 8 - Progetto del Software. 
 | **membri** | **matricola** | **ruolo** |  
 |-----------|:-----------:|-----------:|  
 | Xinxin Ruan | 253062 | Developer |  
 | Alessandro Cornia | 134279 | Product Owner, Developer |
 | Matteo Manocchia | 131056 | Scrum Master, Developer |
 | Laura Morselli | 128602 | Developer |
 | Giovanni Rinaldi | 133276 | Developer |
 | Gianmarco Marella | 128488 | Developer |  

### DESCRIZIONE: 
Lo scopo del progetto è realizzare un software che raccoglie le informazioni in Twitter tramite la ricerca di **hashtag**.
Il progetto è pianificato per essere completato in **4 sprint**.

Il prodotto sarà in grado di geolocalizzare i tweet (**hashtag**) ricercati grazie al aiuto di **Google Maps**; l' **API del twitter** fornirà i metadati degli hashtag individuati, i quali verrano mandati in input alle **API di Google Maps** e produrrà in output la locazione degli hashtag.

Inoltre il software fornirà le funzionalità per i data analyst (**filtri**). In particolare l'utente potrà filtrare gli hashtag in base al tempo.
L'utente sarà in grado di impostare degli **allarmi** in base a diverse condizioni decise da lui. 


Il linguaggio principale preso in considerazione per lo sviluppo backend sarà **JAVA**. Mentre per il frontend verrano considerati **FXML**, **HTML**, **CSS**.  


### LINK:
[Trello](https://trello.com/b/nbWvMNu8/user-story)

[gitlab](https://gitlab.com/gruppo8pds/tweetgrabber)
