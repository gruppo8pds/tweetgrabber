package control;

import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import twitter4j.TwitterException;
import java.util.logging.Logger;

import core.LoginTwitter;

import java.util.logging.Level;

public class LoginController implements Initializable {
	
    private static final Logger LOGGER = Logger.getLogger( LoginController.class.getName() );


	@FXML
	public Button confirm;
	
	@FXML
	private Label label;
	
	@FXML
	private TextField pinText;
	
	@FXML
    private WebView loginPage;

    private LoginTwitter log;
    
	
    private Stage stClose;
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		WebEngine engine;
        try {
			log = new LoginTwitter();
			}
        catch (TwitterException e) {
        	LOGGER.log( Level.FINE, e.toString());
			}
        String url = log.getUrlAccess();
        engine = loginPage.getEngine();
        engine.load(url);
        }
	
	
	public void confermaPin() throws TwitterException{
		String pin = pinText.getText();
		if(!pin.isEmpty())
		{
		log.setToken(pin);
		stClose.close();
		}
		}

	
	public void setStage(Stage st) {
		stClose = st;
		}
	
	public LoginTwitter getLog() {
		return log;
	}
	
}
