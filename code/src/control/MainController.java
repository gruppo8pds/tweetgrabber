package control;

import com.google.gson.*;


import core.MapHandler;
import core.TweetHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import main.Main;
import netscape.javascript.JSObject;
import twitter4j.Status;
import util.AlarmWords;
import util.AnalyzedTweet;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import javax.imageio.ImageIO;


import org.w3c.dom.Element;
import org.w3c.dom.events.EventTarget;

import java.util.logging.Logger;
import java.util.logging.Level;

public class MainController implements Initializable{
    private static final Logger LOGGER = Logger.getLogger( MainController.class.getName() );

    @FXML
	public Tab Analisi;
			
	@FXML
	public Tab Mappa;
	
	@FXML
	public Tab Allarmi;
			
	@FXML
	protected Tab MappaParole;

	@FXML
    protected Label Percent;
	
	@FXML
    public TextField searchBar;
	
    @FXML
    public WebView wordCloudWebView;
    
    @FXML
    public WebView mapView;
    
	@FXML
	public CheckBox cbStream;

    @FXML
    public Button SearchButton;
    
    @FXML
    public Button stopstreambutton;

    @FXML
    public ListView<String> tweetListView;
    
    @FXML
    public Label lblPercent;
  
    @FXML
    public TableView<AnalyzedTweet> tweetStatsTableView;
    @FXML
    public TableColumn<AnalyzedTweet,Integer> favoritedColumn;
    @FXML
    public TableColumn<AnalyzedTweet,Integer> retweetedColumn;
    @FXML
    public TableColumn<AnalyzedTweet,Integer> followersColumn;
    @FXML
    public TableColumn<AnalyzedTweet,Double> influenceColumn;
    @FXML
    public TableColumn<AnalyzedTweet,String> tweetNumberColumn;
    @FXML
    public TableColumn<AnalyzedTweet,String> userColumn;
    @FXML
    public TableColumn<AnalyzedTweet,String> mediaColumn;
    
    @FXML
    public Button login;
    
    @FXML
    public Button condividiPost;
	
	@FXML
	public TextArea alarmDescText;
    
    @FXML
    public TextField alarmWordField;
    
    @FXML
    public Button addWordButton;
    
    @FXML
    public Button removeWordButton;
    
    @FXML 
    private ListView<String> alarmWordsView;
    
    @FXML
   	protected Label language;
   		
   	@FXML
   	protected ChoiceBox <String> choiceBox = new ChoiceBox <>();
   	
   	
    private LoginController loginController;
    private FXMLLoader fxmlLoaderLogin;
    private Parent root2Login;
    private Stage stageLogin;
    
    private JsonArray jsonTweetList;
    private AnalyzedTweet analyzedTweet = new AnalyzedTweet();

    
    private TweetHandler tweetHandler;
    private ObservableList<String> tweetsObsList;
	private ObservableList<String> alarmWordsList;
    
    private MapHandler mapHandler;
    
    //Stringhe usate per impostare la lingua nei vari allarmi del MainController.
    public static String s1 = "Attenzione";
    public static String s2 = "Condivisione non riuscita";
    public static String s3 = "Non hai ancora effettuato l'accesso";
    public static String s4 = "Operazione riuscita";
    public static String s5 = "Il tuo post e' stato publicato con successo";
    public static String s6 = "Selezionare una parola della lista per rimuoverla";
    public static String s7 = "Login gia' effettuato";
    public static String s8 = "Sei gia' pronto per condividere i risultati delle tue ricerche";
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    	mapHandler = new MapHandler(mapView);  
        tweetHandler = new TweetHandler();
        tweetsObsList = FXCollections.observableList(new ArrayList<String>());

        tweetListView.setItems(tweetsObsList);
		
		alarmWordsList = FXCollections.observableList(new ArrayList<String>());
        alarmWordsView.setItems(alarmWordsList);
        
        //getLanguage returns the ObservableList object which you can add items to
        ObservableList<String> list = FXCollections.observableArrayList("IT","US");
         
        choiceBox.setItems(list);
        //Set Fix Value
        choiceBox.setValue("IT");
        
        //Inizializzazione tabella statistiche
        favoritedColumn.setCellValueFactory(new PropertyValueFactory<>("favorited"));
        retweetedColumn.setCellValueFactory(new PropertyValueFactory<>("retweeted"));
        followersColumn.setCellValueFactory(new PropertyValueFactory<>("followers"));
        influenceColumn.setCellValueFactory(new PropertyValueFactory<>("influence"));
        tweetNumberColumn.setCellValueFactory(new PropertyValueFactory<>("tweetNumber"));
        userColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        mediaColumn.setCellValueFactory(new PropertyValueFactory<>("media"));
       
        
        //Gestione evento click su un elemento della lista
        tweetListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                String tweet = tweetListView.getSelectionModel().getSelectedItem();

                //L'utente potrebbe cliccare su una riga vuota
                if (tweet!=null){
                    //Recupero la numerazione del tweet nella lista e rimuovo newline e break
                    tweet = tweet.replace("\n", "").replace("\r", "");
                    int tweetNumber = Integer.parseInt(tweet.replaceAll(":.*","")
                            .replace("#",""));

                    //recupero l'id del tweet dal testo
                    String id = tweet.replaceAll(".*[| id:]","");
                    Status tweetFound = tweetHandler.searchById(id);
                    if (tweetFound!=null){
                        analyzedTweet = new AnalyzedTweet(tweetNumber,tweetFound.getFavoriteCount(),
                                tweetFound.getRetweetCount(),tweetFound.getUser().getFollowersCount(), tweetFound.getUser().getScreenName(), tweetHandler.getM());
                        tweetStatsTableView.getItems().add(analyzedTweet);
                        
                    }
                }

            }
        });
    }
    
    public void loadTweetList(){
    	int tweetCounter;
        if (jsonTweetList!=null){
            
            tweetsObsList.clear();
            tweetStatsTableView.getItems().clear();
            tweetCounter = 0;
            for (JsonElement o: jsonTweetList){
                tweetCounter++;
                String text = "#"+tweetCounter+": "+"\n"+o.getAsJsonObject().get("text").toString() +"\n"+ " || id:"+o.getAsJsonObject().get("id")+"\n";                
                tweetsObsList.add(text);
            }
        }
        
    }
    
   //Ricerca per popolarita'
    public void popularSearch(){
        String searchText = searchBar.getText();

        if (searchText.length()>0){
            //Il metodo search effettua la chiamata a twitter e restituisce una lista di tweet
            jsonTweetList = tweetHandler.search(searchText, mapHandler.getGeoMarkerCoordinate());
            mapHandler.updateJsonTweetList(jsonTweetList);
            mapHandler.updateMap();
            //Chiama i metodi per popolare la lista e creare la cloud word
            loadTweetList();
            createWordCloud();
        }
    }
    
    public void streamSearch(){
        String searchText = searchBar.getText();
        if (searchText.length() > 0){
            tweetListView.getItems().clear();
            tweetStatsTableView.getItems().clear();
            tweetHandler.startStreamSearch(searchText, tweetsObsList);
        }
    }
    
    
    public void stopstreambutton() {
        jsonTweetList = tweetHandler.stopStreamSearch();
        mapHandler.updateJsonTweetList(jsonTweetList);
        mapHandler.updateMap();
        createWordCloud();
        analyzedTweet.percentMedia(tweetHandler.getStreamList(), lblPercent );
    }
    
    public void searchButton(){
    	if(!cbStream.isSelected()) {
    		popularSearch();
    	
    		analyzedTweet.percentMedia(tweetHandler.getPopularList(), lblPercent);
    		
    	}else 
    	{
    		streamSearch();
    	}
    }

    
    public void createWordCloud(){
        //Creo il testo da passare alla funzione per creare la word cloud
        StringBuilder cloudWordText = new StringBuilder();
        for (JsonElement o: jsonTweetList){
            cloudWordText.append(o.getAsJsonObject().get("text").toString());
        }
        String data = cloudWordText.toString();
        
        // crea engine per wordcloud
        WebEngine engine = wordCloudWebView.getEngine();
        engine.load(getClass().getResource("/ui/html/wordCloud.html").toString());
        engine.setJavaScriptEnabled(true);
        
        //Una volta che la pagina e' stata caricata, posso chiamare i metodi per la word cloud
        engine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>(){
            public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState){
                if (newState == Worker.State.SUCCEEDED){
                    JSObject jsObject = (JSObject) engine.executeScript("window");
                    jsObject.call("initialize", data);
                    webViewClickListener(engine, jsObject);
                }
            }
        });
    }
    
    //Questo metodo rileva i click nella cloud word e ne crea un'altra per parole correlate a quella cliccata,
    //utilizzando il testo dei tweet che contengono la parola cliccata.
    private void webViewClickListener(WebEngine engine, JSObject jsObject) {
        Element cloudWordContainer = engine.getDocument().getElementById("container");
        ((EventTarget)cloudWordContainer).addEventListener("click", e -> {
            if (jsonTweetList!=null){
                String clickedWord = engine.executeScript("clickedWord").toString().toLowerCase();
                StringBuilder cloudWordText = new StringBuilder();
                for (JsonElement jsonElement: jsonTweetList){
                    String text = jsonElement.getAsJsonObject().get("text").toString().toLowerCase();
                    //se la parola cliccata nella cloud word �? presente nel tweet, allora lo aggiungo al testo
                    // per la nuova cloud word
                    if (text.contains(clickedWord))
                        cloudWordText.append(text);
                }
                jsObject.call("initialize", cloudWordText.toString());
            }
        }, false);
    }
    
    
    //Crea una finestra di dialogo per il login in twitter
    public void loginT(){
    	if(loginController == null) {
    		try {
    			loginController = new LoginController();
    			fxmlLoaderLogin = new FXMLLoader(getClass().getResource("/ui/fxml/Login.fxml"));
    			fxmlLoaderLogin.setController(loginController);
    			root2Login = fxmlLoaderLogin.load();
    			stageLogin = new Stage();
    			stageLogin.setScene(new Scene(root2Login));  
    			stageLogin.show();
    			stageLogin.getIcons().add(new Image(Main.class.getClassLoader().
    	                getResourceAsStream("ui/fxml/Icon.png")));
    			stageLogin.setTitle("TweetGrabber");
    			stageLogin.setResizable(false);
    	
    			
    			loginController.setStage(stageLogin);
          		}	 
    		catch(Exception e) {
    			LOGGER.log( Level.FINE, e.toString());
        		}
    		}
    	else {
    		if(!loginController.getLog().isLogged()){
    			try {
    				stageLogin.show();
    				loginController.setStage(stageLogin);
    				}
    			catch(Exception e) {
    				LOGGER.log( Level.FINE, e.toString());
        			}
    			}
    		else {
    			Alert alert = new Alert(AlertType.INFORMATION);
            	alert.setTitle(s1);
            	alert.setHeaderText(s7);
            	String s = s8;
            	alert.setContentText(s);
            	alert.show();
    			}
    		}
    	}
    
    
    //effettua uno screenshot che sara'� poi condiviso
    public File getScreen() throws IOException {
    	WritableImage wi = new WritableImage(1189,313);
    	WritableImage writableImage = tweetStatsTableView.snapshot(new SnapshotParameters(), wi);
    	File file = File.createTempFile("screen_tmp", ".png");
    	 try {
             ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
         } catch (IOException ex) {
        	 LOGGER.log( Level.FINE, ex.toString());
         }
    	 return file;
    	}
    
    
    //condivide un post
    public void post() throws IOException{
    	File file = getScreen();
    	loginController.getLog().condividi(file, lblPercent.getText(),searchBar.getText());
    	}
    
    
    //controlla se l'utente e' loggato e nel caso permette la condivisione
    public void postControll() throws IOException {
    	if((loginController == null) || !(loginController.getLog().isLogged())){
    		Alert alert = new Alert(AlertType.INFORMATION);
        	alert.setTitle(s1);
        	alert.setHeaderText(s2);
        	String s = s3;
        	Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        	stage.getIcons().add(new Image(this.getClass().getResource("/ui/fxml/Icon.png").toString()));
        	
        	alert.setContentText(s);
        	alert.show();	
    		}
    	else {
    		post();
    		Alert alert = new Alert(AlertType.INFORMATION);
        	alert.setHeaderText(s4);
        	String s =s5;
        	Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        	stage.getIcons().add(new Image(this.getClass().getResource("/ui/fxml/Icon.png").toString()));
        	alert.setContentText(s);
        	alert.show();
    		}
    	
    	}
		
		public void addAlarmWord() {
    	
    	String word = alarmWordField.getText();
    	
    	if (!word.isEmpty()) {
    	
    		AlarmWords.alarmWords.addAlarmWord(word);
    	
    		alarmWordsList.add(word);
    	
    		alarmWordField.setText("");
    		
    	}
 
    }
    
    public void removeAlarmWord() {
    	
    	try {
    		
	    	String word = alarmWordsView.getSelectionModel().getSelectedItem();
	    	
	    	if (!word.isEmpty()) {
	    	
	    		AlarmWords.alarmWords.removeAlarmWord(word);
	    	
	    		alarmWordsList.remove(word);
	    		
	    	}
	    	
    	} catch(NullPointerException e) {
    		
    		Alert alert = new Alert(AlertType.INFORMATION);
        	alert.setTitle(s1);
        	alert.setHeaderText("");
        	String s = s6;
        	Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        	stage.getIcons().add(new Image(this.getClass().getResource("/ui/fxml/Icon.png").toString()));
        	alert.setContentText(s);
        	alert.show();
       
    	}
    }
    

    public void changeCombo(ActionEvent event)
    {
    	
    	String s = choiceBox.getValue();
    	
    		util.Linguaggio lingua;
            lingua = new util.Linguaggio();
            lingua.leggiToken(s);
            Analisi.setText(lingua.getAnalisi());
            cbStream.setText(lingua.getcbStream());
            favoritedColumn.setText(lingua.getfavoritedColumn());
            followersColumn.setText(lingua.getfollowersColumn());
            influenceColumn.setText(lingua.getinfluenceColumn());
            language.setText(lingua.getlanguage());
            Mappa.setText(lingua.getMappa());
            MappaParole.setText(lingua.getMappaParole());
            mediaColumn.setText(lingua.getmediaColumn());
            Percent.setText(lingua.getPercent());
            retweetedColumn.setText(lingua.getretweetedColumn());
            searchBar.setPromptText(lingua.getsearchBar());
            SearchButton.setText(lingua.getSearchButton());
            stopstreambutton.setText(lingua.getstopstreambutton());
            tweetNumberColumn.setText(lingua.gettweetNumberColumn());
            userColumn.setText(lingua.getuserColumn()); 
    	    Allarmi.setText(lingua.getAllarms());
    	    alarmDescText.setText(lingua.getAllarmsText());
    	    alarmWordField.setPromptText(lingua.getAllarmsField());
    	    addWordButton.setText(lingua.getAllarmsAdd());
    	    removeWordButton.setText(lingua.getAllarmsRemove());
    	    condividiPost.setText(lingua.getcondividiPost());
    	    s1 = (lingua.gets1());
    	    s2 = (lingua.gets2());
    	    s3 = (lingua.gets3());
    	    s4 = (lingua.gets4());
    	    s5 = (lingua.gets5());
    	    s6 = (lingua.gets6());
    	    s7 = (lingua.gets7());
    	    s8 = (lingua.gets8());
  
    }
    
    
}

