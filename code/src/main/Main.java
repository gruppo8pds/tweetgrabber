package main;

import control.MainController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	private Stage currentStage;
	private static MainController mainController;
	private static FXMLLoader graficaloader, maploader;
	
    @Override
    public void start(Stage primaryStage) throws Exception{
    	// inizializzazione variabili
    	currentStage = primaryStage;
    	mainController = new MainController();
  
    	graficaloader = new FXMLLoader(getClass().getResource("/ui/fxml/grafica.fxml"));
    	graficaloader.setController(mainController);
    	
        Parent root = graficaloader.load();
        primaryStage.getIcons().add(new Image(Main.class.getClassLoader().
                getResourceAsStream("ui/fxml/Icon.png")));
       
        
        primaryStage.setTitle("TweetGrabber");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest((ae -> {
            Platform.exit();
            System.exit(0);
        }));
    }

    public static void main(String[] args) {
        launch(args);
    }
    public Stage getCurrentStage() {return currentStage;}
    public static MainController getMainController() {return mainController;}
    public static FXMLLoader getMapFXML() {return maploader;}
}
