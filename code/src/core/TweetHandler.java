package core;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import control.MainController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import util.Configurazione;
import util.Position;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.swing.JOptionPane;


public class TweetHandler {

    private final ConfigurationBuilder cb;
    private TwitterStream twitterStream;
    private Twitter twitter;
    private List<Status> streamTweets = new ArrayList<>();
    private Gson gson;
    private boolean m;
    private List<Status> tweetList = new ArrayList<>();
    private AlarmHandler alarm = new AlarmHandler();
    private MainController mc = new MainController();

    
    private static final Logger LOGGER = Logger.getLogger( TweetHandler.class.getName() );
    

    Configurazione conf;

    public TweetHandler(){
    	conf = new Configurazione();
    	gson = new Gson();
        cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(conf.getConsumerKey())
                .setOAuthConsumerSecret(conf.getConsumerSecret())
                .setOAuthAccessToken(conf.getAccessToken())
                .setOAuthAccessTokenSecret(conf.getAccessTokenSecretn())
                .setTweetModeExtended(true);

        Configuration config = cb.build();
        //inizializzazione twitter stream
        twitterStream = new TwitterStreamFactory(config).getInstance();
        //inizializzazione twitter search
        twitter = new TwitterFactory(config).getInstance();
    }

   public void startStreamSearch(String searchText, ObservableList<String> tweetsObsList){
        //L'utente puo' inserire piu' parole separate da una virgola
        String[] keywords = searchText.toLowerCase().split(",");
        alarm.setEventStartTime(System.currentTimeMillis());
        StatusListener listener = new StatusListener() {
            int count = 0;
            long updateTweetListTime;
            boolean alarmShown = false;
            @Override
            public void onStatus(Status status) {
                //Il filtro per le parole va inserito qui se si usano altri filtri perche' la filter query combina i filtri
                // con un "OR", cioe' ottengo tweet in Italia OR tweet con le parole chiave scelte. Io voglio un AND.
                String statusText = status.getText();
				
                if (Arrays.stream(keywords).parallel().anyMatch(statusText.toLowerCase()::contains)){
                    count++;
                    String text = "#" + count + ": " +"\n" + statusText +"\n"+ " || id:"+status.getId()+"\n";
                    streamTweets.add(status);
                    //L'interfaccia utente non puo' essere aggiornata direttamente da un thread che non fa parte
                    //dell'applicazione quindi e' necessario usare questo metodo. Questo sistema va modificato in quanto
                    //non e' corretto che sia una classe diversa dal controller a gestire l'aspetto di un'interfaccia.
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            //Aggiorno listview
                            tweetsObsList.add(text);
                            updateTweetListTime = System.currentTimeMillis();
                            
                            for (String keyword : keywords) {
                            	if (!alarmShown && alarm.checkAlarmCondition(keyword, count, updateTweetListTime)) {
                            	
                            		boolean shareAlarmResult = alarm.activateAlarm();
                            	
                            		if (shareAlarmResult) {
                            			try {
                            				mc.postControll();
                            			}
                            			catch(IOException ex) {
                            				JOptionPane.showMessageDialog(null, 
                    								"<html><body><p style='width: 500px;'>Si e' verificato un errore "
                    								+ "durante la pubblicazione dei risultati.</p></body></html>", 
                    								"Errore", JOptionPane.ERROR_MESSAGE);
                            			}

                            		}
                            	
                            		alarmShown = true;
                            	}
                            }
                            
                        }
                    
                    });
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            	//Inutilizzato
            }

            @Override
            public void onTrackLimitationNotice(int i) {
            	//Inutilizzato
            }

            @Override
            public void onScrubGeo(long l, long l1) {
            	//Inutilizzato
            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {
            	//Inutilizzato
            }

            @Override
            public void onException(Exception e) {
            	//Inutilizzato
            }
            
        };
        twitterStream.addListener(listener);
        FilterQuery streamfilter = new FilterQuery();
        //se l'utente vuole tweet geolocalizzati allora uso la boundary box
        
        streamfilter.track(keywords);
        
        
        twitterStream.filter(streamfilter);
        
    }
    
    //Chiude la comunicazione con i server di Twitter e restituisce l'array JSON dei tweet
    public JsonArray stopStreamSearch(){
        twitterStream.cleanUp();
        twitterStream.shutdown();
        return gson.toJsonTree(streamTweets).getAsJsonArray();
    }

    //Implementa la ricerca per popolarit�
    public JsonArray search(String searchTerm, Position geomarkerCoordinate){
    	tweetList.clear();
    	tweetList = new ArrayList<>();
        Query query = new Query(searchTerm);
        //100 e' il numero massimo di tweet ottenibili con le API standard
        query.setCount(100);
        
        //Ricerca in base al Geocode, con un radius di 500km dalla coordinata fornita
        query.setGeoCode(new GeoLocation(geomarkerCoordinate.getLatitude(), geomarkerCoordinate.getLongitude()), 500, Query.KILOMETERS);
        
        
        try {
            tweetList = twitter.search(query).getTweets();
        } catch (TwitterException e) {
        	LOGGER.log( Level.FINE, e.toString());
        }
        return gson.toJsonTree(tweetList).getAsJsonArray();
    }
    
    //Dato l'id di un tweet, restituisce il tweet a cui corrisponde
    public Status searchById(String id){
    	m=false;
        Status status = null;
        MediaEntity[] t = null;
        try {
            status = twitter.showStatus(Long.parseLong(id));
            if (status == null) {
            	LOGGER.log( Level.FINE,"Tweet not found");
            } else {
               
                t = status.getMediaEntities();
                if (t.length!=0) {
                	m=true;
                }
            }
        } catch (TwitterException e) {
        	LOGGER.log( Level.WARNING, e.getMessage());
            LOGGER.log( Level.FINE, e.toString());
        }
        return status;
    }
    
    public List<Status> getStreamList(){
    	return streamTweets;
    }
    
    public List<Status> getPopularList(){
    	return tweetList;
    }
    
    public boolean getM() {
    	return m;
    }
    

    


}
