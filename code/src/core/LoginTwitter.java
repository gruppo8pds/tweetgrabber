package core;

import java.io.File;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.UploadedMedia;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import java.util.logging.Logger;

import java.util.logging.Level;

public class LoginTwitter {
	
    private static final Logger LOGGER = Logger.getLogger( LoginTwitter.class.getName() );


	private Twitter twitter2;
	private RequestToken requestToken;
	private AccessToken accessToken;
	
	
	public LoginTwitter() throws TwitterException {
		twitter2 = TwitterFactory.getSingleton();
    	twitter2.setOAuthConsumer("OE6SZBtdHyum7ymXT3lPEhYyj", "5tmxhOonsjhSoWDVKeIro689ETPZmbnB9g6RiYIn5yuUZnMhr0");
    	requestToken = twitter2.getOAuthRequestToken();
    	accessToken = null;
	}
	
	
	public String getUrlAccess() {
		return requestToken.getAuthorizationURL();
		}
	
	public void setToken(String pin){
		while (null == accessToken) {
	    	  try{
	    		  if(pin.length() > 0)
	    			  accessToken = twitter2.getOAuthAccessToken(requestToken, pin);
	    		  else
	    			  accessToken = twitter2.getOAuthAccessToken();
	    	  }
	    		  catch (TwitterException te) {
		    			Alert alert = new Alert(AlertType.INFORMATION);
		            	alert.setTitle("Attenzione");
		            	alert.setHeaderText("Login non effettuato");
		            	String s ="Errore di accesso";
		            	alert.setContentText(s);
		            	alert.show();
		    	  
	            	Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
	            	stage.getIcons().add(new Image(this.getClass().getResource("/ui/fxml/Icon.png").toString()));
	    	       }
		      }
    }
	
	
	public boolean isLogged() {
		if(accessToken != null)
			return true;
		return false;
	}
	
	
	public void condividi(File imagefile, String label, String hastag) {
		try{
			String messaggio = "Abbiamo appena analizzato l'hashtag #" + hastag + ".\n" + 
					"I tweet contenenti foto e video sono: " + label + ".\n" + 
					"Ecco altri risultati utili: ";
            
           
           	long[] mediaIds = new long[1];
           	UploadedMedia media = twitter2.uploadMedia(imagefile);
           	mediaIds[0] = media.getMediaId();
           	StatusUpdate statusUpdate = new StatusUpdate(messaggio);
           	statusUpdate.setMediaIds(mediaIds);
           	
            
            
            twitter2.updateStatus(statusUpdate);
            
        }
		catch (TwitterException te) {
			LOGGER.log( Level.FINE, te.toString());
            System.exit(-1);
        }	
	}
}