package core;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import twitter4j.GeoLocation;
import util.Position;


public class MapHandler{
	//*********** Variabile **********************//
    @FXML
    public WebView mapView;

    

    private List<Position> positionList;
    
    private JSObject documentHTML;

    private JsonArray jsonTweetList;
    private Gson gson;    
    //************* CONSTRUCTOR *******************//
    public MapHandler(WebView mapV) {
    	mapView = mapV;
    	gson = new Gson();
    	positionList = new ArrayList<Position>();
    	initializeEngine();
    }
    
    public MapHandler() {
    	gson = new Gson();
    	positionList = new ArrayList<Position>();
    }
    
    private void initializeEngine() {
    	WebEngine mapEngine;
    	initializeTweetPositions();
    	
    	mapEngine = mapView.getEngine();

        mapEngine.load(getClass().getResource("/ui/html/map.html").toString());
        mapEngine.setJavaScriptEnabled(true);
        documentHTML = (JSObject) mapEngine.executeScript("window");
        documentHTML.setMember("app", this);        
    }
       
    //Dato un campo GeoLocation e una descrizione provenienti da un tweet, restituisce l'oggetto position corrispondente
    public Position toPosition(GeoLocation[][] geoLocations, String tweetText) {
        //getBoundingBoxCoordinates restituisce una matrice 1x4, quindi [0][0],[0][1],[0][2],[0][3]
        //formula centro per n punti: (x1+x2+x3+x4)/4,(y1,y2,y3,y4)/4
        double latitude = (geoLocations[0][0].getLatitude()+geoLocations[0][1].getLatitude()
                +geoLocations[0][2].getLatitude()+geoLocations[0][3].getLatitude())/4;
        double longitude = (geoLocations[0][0].getLongitude()+geoLocations[0][1].getLongitude()
                +geoLocations[0][2].getLongitude()+geoLocations[0][3].getLongitude())/4;
        return new Position(latitude,longitude,tweetText);
    }
    
    public void updateMap() {
    	initializeTweetPositions();
        documentHTML.call("clearMarkers");
        for (Position position: positionList){
              documentHTML.call("addMarker", position.getLatitude(), position.getLongitude(), position.getDescription());
        }
    }
    
    public void updateJsonTweetList(JsonArray jsont) {
    	jsonTweetList = jsont;
    }
    
    //inizializza la lista positions che verra'  letta alla mappa per generare i marker
    public void initializeTweetPositions(){
        if (jsonTweetList!=null){
        	// inizializza
            JsonObject jsonGeoLocation;
            String tweetText;
            //svuota il positionList
            positionList.clear();

            for (Object jsonTweet: jsonTweetList) {
                JsonObject jsonPlace = (JsonObject) ((JsonObject)jsonTweet).get("place");
                
                jsonGeoLocation = (JsonObject) ((JsonObject) jsonTweet).get("geoLocation");

                if (jsonGeoLocation != null){
                		tweetText = ((JsonObject) jsonTweet).get("text").getAsString();
                        double latitude = Double.parseDouble(jsonGeoLocation.get("latitude").toString());
                        double longitude = Double.parseDouble(jsonGeoLocation.get("longitude").toString());
                       
                        
                        positionList.add(new Position(latitude,longitude, tweetText));
                }
                
                else if (jsonPlace != null){
                		tweetText = ((JsonObject) jsonTweet).get("text").getAsString();
                		JsonArray jsonPlaceString = jsonPlace.get("boundingBoxCoordinates").getAsJsonArray();
                        GeoLocation[][] geoLocations = gson.fromJson(jsonPlaceString, GeoLocation[][].class);
                        positionList.add(toPosition(geoLocations, tweetText));
                }
            }
        }
    }
    
    public List<Position> getPositionList(){return positionList;}
    
    public Position getGeoMarkerCoordinate() {
    	// prendi il valore della coordinata in stringa
    	String jsobjectCoordinate = (String)documentHTML.call("getGeoMarker_coordinate");
    	
    	// !! --- string parsing ----
    	String[] strCoordinate = jsobjectCoordinate.split(",");
    	String strLat = strCoordinate[0].substring(strCoordinate[0].lastIndexOf(":")+1);
    	String strLng = strCoordinate[1].substring(strCoordinate[1].lastIndexOf(":")+1);

    	double latitude = Double.parseDouble(strLat);
    	double longitude = Double.parseDouble(strLng);
    	return new Position(latitude, longitude, "geomarker");
    }
}

