package core;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import util.AlarmWords;
import util.Configurazione;
import util.Linguaggio;



public class AlarmHandler {
	
	Configurazione cf = new Configurazione();
	
	
	private long eventStartTime;
	private int numTweets;
	private long secondsPassed;
	
	public long getEventStartTime() {
		
		return this.eventStartTime;
	}
	
	public void setEventStartTime(long eventStartTime) {
		
		this.eventStartTime = eventStartTime;
		
	}
	
	public boolean checkAlarmCondition(String word, int numTweets, long eventUpdateTime) {
		
		if (AlarmWords.alarmWords.getAlarmWordsList().contains(word)) {
			
			long timePassed = eventUpdateTime - eventStartTime;
		
			if (numTweets > cf.getNumTweets() && timePassed < cf.getMillisPassed()) {
				this.numTweets = numTweets;
				this.secondsPassed = (int) (timePassed / 1000) % 60;
				return true;
			}
		}

		return false;
	}
	

public boolean activateAlarm() {
		
		String[] buttons = {Linguaggio.getCondividi(), Linguaggio.getIgnora()};
		ImageIcon icon = new ImageIcon("/ui/fxml/Icon.png");
    	int choice = JOptionPane.showOptionDialog(null, 
				"<html><body><p style='width: 500px;'>Sono stati pubblicati piu' di " + this.numTweets 
				+ " tweet in meno di " + this.secondsPassed + " secondi.</p></body></html>", 
				"Allarme", JOptionPane.WARNING_MESSAGE, 0, icon, buttons, buttons[0]);
    	
    	if (choice == 0)
    		return true;
    	else
    		return false;
		
	}

}
