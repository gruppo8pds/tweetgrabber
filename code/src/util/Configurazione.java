package util;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Configurazione {
    private  String consumerKey;
    private  String consumerSecret;
    private  String accessToken;
    private  String accessTokenSecret;
    private int numTweetsAlarmCondition;
    private int millisPassedAlarmCondition;
    
    private static final Logger LOGGER = Logger.getLogger( Configurazione.class.getName() );
    
    public Configurazione() {
    	leggiToken();
    }
    
    public void leggiToken() {
    	try 
    		(BufferedReader reader = new BufferedReader(new FileReader("code/config/key/conf.txt")))
    		{
    		consumerKey = reader.readLine();
    		consumerSecret = reader.readLine();
    		accessToken = reader.readLine();
    		accessTokenSecret = reader.readLine();
    		numTweetsAlarmCondition = Integer.parseInt(reader.readLine());
    		millisPassedAlarmCondition = Integer.parseInt(reader.readLine());
    		

    	} catch(IOException e) {
    		LOGGER.log( Level.FINE, e.toString());
    	}
    }
    
    public int  getNumTweets() {
    	
    	return numTweetsAlarmCondition;
    }
    
    public int  getMillisPassed() {
    	
    	return millisPassedAlarmCondition;
    }
    
    public String getConsumerKey() {
    	return consumerKey;
    }
    
    public String getConsumerSecret() {
    	return consumerSecret;
    }
    
    public String getAccessToken() {
    	return accessToken;
    }
    
    public String getAccessTokenSecretn() {
    	return accessTokenSecret;
    }
}