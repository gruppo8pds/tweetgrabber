package util;

public class BoundingBox {
	
    //coordinate di Roma
    private double latitude;
    private double longitude;
    private double radius;
    //crea due nuove coppie di coordinate che indicano due angoli opposti del quadrato che delimita l'area
    //in cui ricerco i tweet. Per farlo mi basero' sulle coordinate di Roma.

    
    
    public BoundingBox(double latit, double longit) {
    	setLatitude(latit);
    	setLongitude(longit);
    }
    
    public BoundingBox(Position position, double rad) {
    	setLatitude(position.getLatitude());
    	setLongitude(position.getLongitude());
    	setRadius(rad);
    }
    
    double lat1 = latitude - 6;
    double long1 = longitude - 6;
    double lat2 = latitude + 5;
    double long2 = longitude + 6;
    double[][] italyBB = {{long1, lat1}, {long2, lat2}};
    
    public double[][] getBoundingBox() {
    	double[][] boundingbox;
        double latitudeX = latitude - radius;
        double longitudeX = longitude - radius;
        double latitudeY = latitude + radius;
        double longitudeY = longitude + radius;
        boundingbox = new double[][]{{longitudeX, latitudeX},{longitudeY, latitudeY}};
        return boundingbox;
    }
    public void setRadius(double r) {
    	radius = r;
    }
    public void setLatitude(double l) {
    	latitude = l;
    }
    
    public void setLongitude(double l) {
    	longitude = l;
    }
}
