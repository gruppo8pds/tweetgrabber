package util;

import java.util.ArrayList;

public class AlarmWords {
	
	public static final AlarmWords alarmWords = new AlarmWords();
	
	private ArrayList<String> alarmWordsList;
	
	public AlarmWords() {
		
		this.alarmWordsList = new ArrayList<>();
	}
	
	public void addAlarmWord(String word) {
		
		alarmWordsList.add(word);
		
	}
	
	public void removeAlarmWord(String word) {
		
		alarmWordsList.remove(word);
	}
	
	public ArrayList<String> getAlarmWordsList() {
		
		return alarmWordsList;
	}

}
