package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;


import java.util.logging.Level;

public class Linguaggio {
    private  String stopstreambutton;
    private  String cbStream;
    private  String searchBar;
    private  String SearchButton;
    private  String Analisi;
    private  String Mappa;
    private  String MappaParole;
    private  String language;
    private  String Percent;
    private  String tweetNumberColumn;
    private  String userColumn;
    private  String favoritedColumn;
    private  String retweetedColumn;
    private  String followersColumn;
    private  String influenceColumn;
    private  String mediaColumn;
    private  String allarms;
    private  String AllarmsText;
    private  String AllarmsField;
    private  String AllarmsAdd;
    private  String AllarmsRemove;
    private  String condividiPost;
    private static String Condividi;
    private static String Ignora;
    private  String s1;
    private  String s2;
    private  String s3;
    private  String s4;
    private  String s5;
    private  String s6;
    private  String s7;
    private  String s8;
   
    private static final Logger LOGGER2 = Logger.getLogger( Linguaggio.class.getName() );
    
    public Linguaggio() {

    	stopstreambutton=null;
        cbStream=null;
        searchBar=null;
        SearchButton=null;
        Analisi=null;
        Mappa=null;
        MappaParole=null;
        language=null;
        Percent=null;
        tweetNumberColumn=null;
        userColumn=null;
        favoritedColumn=null;
        retweetedColumn=null;
        followersColumn=null;
        influenceColumn=null;
        mediaColumn=null;
        allarms=null;
        AllarmsText=null;
        AllarmsField=null;
        AllarmsAdd=null;
        AllarmsRemove=null;
        condividiPost=null;
        s1=null;
        s2=null;
        s3=null;
        s4=null;
        s5=null;
        s6=null;
        s7=null;
        s8=null;
       
        
    }
    
    public void leggiToken(String s) {
    	try 
    		(BufferedReader reader = new BufferedReader(new FileReader("code/config/language/"+s+".txt")))
    		{
    		stopstreambutton = reader.readLine();
    		cbStream = reader.readLine();
    		searchBar = reader.readLine();
    		SearchButton = reader.readLine();
    		Analisi = reader.readLine();
    		Mappa = reader.readLine();
    		MappaParole = reader.readLine();
    		language = reader.readLine();
    		Percent = reader.readLine();
    		tweetNumberColumn = reader.readLine();
    		userColumn = reader.readLine();
    		favoritedColumn = reader.readLine();
    		retweetedColumn = reader.readLine();
    		followersColumn = reader.readLine();
    		influenceColumn = reader.readLine();
    		mediaColumn = reader.readLine();
    		allarms = reader.readLine();
    		AllarmsText = reader.readLine();
            AllarmsField = reader.readLine();
            AllarmsAdd = reader.readLine();
            AllarmsRemove = reader.readLine();
            condividiPost = reader.readLine();
            s1 = reader.readLine();
            s2 = reader.readLine();
            s3 = reader.readLine();
            s4 = reader.readLine();
            s5 = reader.readLine();
            s6 = reader.readLine();
            s7 = reader.readLine();
            s8 = reader.readLine();
            Condividi = reader.readLine();
            Ignora = reader.readLine();
            

    	} catch(IOException e) {
    		LOGGER2.log( Level.FINE, e.toString());
    	}
    }
    
    public String getstopstreambutton() {
    	return stopstreambutton;
    }
    
    public String getcbStream() {
    	return cbStream;
    }
    
    public String getsearchBar() {
    	return searchBar;
    }
    
    public String getSearchButton() {
    	return SearchButton;
    }
    public String getAnalisi() {
    	return Analisi;
    }
    
    public String getMappa() {
    	return Mappa;
    }
    
    public String getMappaParole() {
    	return MappaParole;
    }
    
    public String getlanguage() {
    	return language;
    }
    public String getPercent() {
    	return Percent;
    }
    
    public String gettweetNumberColumn() {
    	return tweetNumberColumn;
    }
    
    public String getuserColumn() {
    	return userColumn;
    }
    
    public String getfavoritedColumn() {
    	return favoritedColumn;
    }
    public String getretweetedColumn() {
    	return retweetedColumn;
    }
    
    public String getfollowersColumn() {
    	return followersColumn;
    }
    
    public String getinfluenceColumn() {
    	return influenceColumn;
    }
    
    public String getmediaColumn() {
    	return mediaColumn;
    }
    
    public String getAllarms() {
    	return allarms;
    }
    
    public String getAllarmsText() {
    	return AllarmsText;
    }
    public String getAllarmsField() {
    	return AllarmsField;
    }
    public String getAllarmsAdd() {
    	return AllarmsAdd;
    }
    public String getAllarmsRemove() {
    	return AllarmsRemove;
    }
    public String getcondividiPost() {
    	return condividiPost;
    }
    public String gets1() {
    	return s1;
    }
    public String gets2() {
    	return s2;
    }
    public String gets3() {
    	return s3;
    }
    public String gets4() {
    	return s4;
    }
    public String gets5() {
    	return s5;
    }
    public String gets6() {
    	return s6;
    }
  
    public String gets7() {
    	return s7;
    }
    public String gets8() {
    	return s8;
    }
    public static String getCondividi() {
    	return Condividi;
    }
    public static String getIgnora() {
    	return Ignora;
    }
}
