package util;

import java.util.List;
import javafx.scene.control.Label;
import twitter4j.MediaEntity;
import twitter4j.Status;

public class AnalyzedTweet {

    private int tweetNumber;
    private int favorited;
    private int retweeted;
    private int followers;
    private String name;
    private double influence;
    private boolean media;

   
    public AnalyzedTweet(int tweetNumber, int favorited, int retweeted, int followers, String name, boolean media){
    	
    	this.media = media;
    	this.name = name;
        this.tweetNumber = tweetNumber;
        this.favorited = favorited;
        this.retweeted = retweeted;
        this.followers = followers;
        //Il valore del campo influence e' calcolato secondo parametri arbitrari. Sarebbe utile una ricerca approfondita
        //per trovare un modo per calcolare questo valore e dargli maggiore significativita' .
        influence = favorited*1.25+retweeted*1.75+followers*0.25;
    }
    
    public AnalyzedTweet() {
    	
    }
    
    public void percentMedia(List<Status> list, Label lblPercent) {
    	int n = 1;
    	int cont = 0;
    	MediaEntity[] t = null;
    	if(list!=null) {
		    	for (Status tweetFound : list ) {
		    		n++;
		            t = tweetFound.getMediaEntities();
		            if (t.length!=0) {
		            	cont++;
		            }
		    	}
    	
    		int percent = cont*100/n;
    		lblPercent.setText(Double.toString(percent)+ " %");
    	
    	}else {
    		lblPercent.setText("0 %");
    	}
    }
    
    

    
    public String getMedia() {
    	if(this.media) {
    		return "\t Si";
    	}else {
    		return "No Foto/Video";
    	}
    }
    
    public String getName() {
    	return name;
    }
    
    public void setName(String name) {
    	this.name = name;
    }

    public int getTweetNumber() {
        return tweetNumber;
    }

    public void setTweetNumber(int tweetNumber) {
        this.tweetNumber = tweetNumber;
    }

    public int getFavorited() {
        return favorited;
    }

    public void setFavorited(int favorited) {
        this.favorited = favorited;
    }

    public int getRetweeted() {
        return retweeted;
    }

    public void setRetweeted(int retweeted) {
        this.retweeted = retweeted;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public double getInfluence() {
        return influence;
    }

    public void setInfluence(double influence) {
        this.influence = influence;
    }

}
